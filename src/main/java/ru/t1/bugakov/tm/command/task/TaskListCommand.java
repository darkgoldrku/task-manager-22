package ru.t1.bugakov.tm.command.task;

import ru.t1.bugakov.tm.enumerated.TaskSort;
import ru.t1.bugakov.tm.model.Task;
import ru.t1.bugakov.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

public final class TaskListCommand extends AbstractTaskCommand {

    @Override
    public void execute() {
        System.out.println("[TASK LIST]");
        System.out.println("ENTER SORT:");
        System.out.println(Arrays.toString(TaskSort.values()));
        final String sortType = TerminalUtil.nextLine();
        final String userId = getAuthService().getUserId();
        final TaskSort sort = TaskSort.toSort(sortType);
        final List<Task> tasks = getTaskService().findAll(userId, sort.getComparator());
        int index = 1;
        for (final Task task : tasks) {
            if (task == null) continue;
            System.out.println(index + ". " + task.getName());
            index++;
        }
    }

    @Override
    public String getName() {
        return "task-list";
    }

    @Override
    public String getDescription() {
        return "Show list tasks.";
    }

}

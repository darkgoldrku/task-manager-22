package ru.t1.bugakov.tm.command.user;

import ru.t1.bugakov.tm.enumerated.Role;
import ru.t1.bugakov.tm.util.TerminalUtil;

public final class UserRemoveCommand extends AbstractUserCommand {

    @Override
    public void execute() {
        System.out.println("[USER REMOVE]");
        System.out.println("ENTER LOGIN:");
        final String login = TerminalUtil.nextLine();
        getUserService().removeByLogin(login);
    }

    @Override
    public String getName() {
        return "user-remove";
    }

    @Override
    public String getDescription() {
        return "Remove user.";
    }

    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}

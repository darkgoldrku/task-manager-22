package ru.t1.bugakov.tm.service;

import ru.t1.bugakov.tm.api.repository.IAbstractRepository;
import ru.t1.bugakov.tm.api.service.IAbstractService;
import ru.t1.bugakov.tm.exception.entity.ModelNotFoundException;
import ru.t1.bugakov.tm.exception.field.IdEmptyException;
import ru.t1.bugakov.tm.exception.field.IndexIncorrectException;
import ru.t1.bugakov.tm.model.AbstractModel;

import java.util.Comparator;
import java.util.List;

public abstract class AbstractService<M extends AbstractModel, R extends IAbstractRepository<M>> implements IAbstractService<M> {

    protected final R repository;

    protected AbstractService(final R repository) {
        this.repository = repository;
    }

    @Override
    public M add(final M model) {
        if (model == null) throw new ModelNotFoundException();
        return repository.add(model);
    }

    @Override
    public List<M> findAll() {
        return repository.findAll();
    }

    @Override
    public List<M> findAll(final Comparator<M> comparator) {
        if (comparator == null) return findAll();
        return repository.findAll(comparator);
    }

    @Override
    public int getSize() {
        return repository.getSize();
    }

    @Override
    public M findById(final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.findById(id);
    }

    @Override
    public M findByIndex(final Integer index) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        return repository.findByIndex(index);
    }

    @Override
    public boolean existsById(final String id) {
        return findById(id) != null;
    }

    @Override
    public void clear() {
        repository.clear();
    }

    @Override
    public M remove(final M model) {
        repository.remove(model);
        return model;
    }

    @Override
    public M removeById(final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.removeById(id);
    }

    @Override
    public M removeByIndex(final Integer index) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        return repository.removeByIndex(index);
    }

}

package ru.t1.bugakov.tm.api.repository;

import ru.t1.bugakov.tm.model.AbstractModel;

import java.util.Comparator;
import java.util.List;

public interface IAbstractRepository<M extends AbstractModel> {

    M add(M model);

    List<M> findAll();

    List<M> findAll(Comparator<M> comparator);

    int getSize();

    M findById(String id);

    M findByIndex(Integer index);

    boolean existsById(String id);

    void clear();

    M remove(M model);

    M removeById(String id);

    M removeByIndex(Integer index);

}

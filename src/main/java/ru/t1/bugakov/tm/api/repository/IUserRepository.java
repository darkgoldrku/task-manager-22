package ru.t1.bugakov.tm.api.repository;

import ru.t1.bugakov.tm.model.User;

public interface IUserRepository extends IAbstractRepository<User> {

    User findByLogin(String login);

    User findByEmail(String email);

    Boolean isLoginExist(String login);

    Boolean isEmailExist(String email);

    User lockUserByLogin(String login);

    User unlockUserByLogin(String login);

}

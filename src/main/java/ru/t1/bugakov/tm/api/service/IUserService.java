package ru.t1.bugakov.tm.api.service;

import ru.t1.bugakov.tm.enumerated.Role;
import ru.t1.bugakov.tm.model.User;

public interface IUserService extends IAbstractService<User> {

    User create(String login, String password);

    User create(String login, String password, String email);

    User create(String login, String password, Role role);

    User findByLogin(String login);

    User findByEmail(String email);

    User removeByLogin(String login);

    User removeByEmail(String email);

    User setPassword(String id, String password);

    User updateUser(String id,
                    String firstName,
                    String lastName,
                    String middleName);

    Boolean isLoginExist(String login);

    Boolean isEmailExist(String email);

    User lockUserByLogin(String login);

    User unlockUserByLogin(String login);
}

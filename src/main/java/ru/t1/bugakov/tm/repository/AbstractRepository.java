package ru.t1.bugakov.tm.repository;

import ru.t1.bugakov.tm.api.repository.IAbstractRepository;
import ru.t1.bugakov.tm.model.AbstractModel;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public abstract class AbstractRepository<M extends AbstractModel> implements IAbstractRepository<M> {

    protected final List<M> records = new ArrayList<>();

    @Override
    public M add(final M model) {
        records.add(model);
        return model;
    }

    @Override
    public List<M> findAll() {
        return records;
    }

    @Override
    public List<M> findAll(final Comparator<M> comparator) {
        return records
                .stream()
                .sorted(comparator)
                .collect(Collectors.toList());
    }

    @Override
    public int getSize() {
        return (int) records
                .stream()
                .count();
    }

    @Override
    public M findById(final String id) {
        return records
                .stream()
                .filter(m -> id.equals(m.getId()))
                .findFirst().orElse(null);
    }

    @Override
    public M findByIndex(final Integer index) {
        return records
                .stream()
                .skip(index)
                .findFirst().orElse(null);
    }

    @Override
    public boolean existsById(final String id) {
        return findById(id) != null;
    }

    @Override
    public void clear() {
        records.clear();
    }

    @Override
    public M remove(final M model) {
        records.remove(model);
        return model;
    }

    @Override
    public M removeById(final String id) {
        final M model = findById(id);
        if (model == null) return null;
        records.remove(model);
        return model;
    }

    @Override
    public M removeByIndex(final Integer index) {
        final M model = findByIndex(index);
        if (model == null) return null;
        records.remove(model);
        return model;
    }

}
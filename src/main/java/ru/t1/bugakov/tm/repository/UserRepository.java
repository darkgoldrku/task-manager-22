package ru.t1.bugakov.tm.repository;

import ru.t1.bugakov.tm.api.repository.IUserRepository;
import ru.t1.bugakov.tm.model.User;

public final class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @Override
    public User findByLogin(final String login) {
        return records
                .stream()
                .filter(m -> login.equals(m.getLogin()))
                .findFirst().orElse(null);
    }

    @Override
    public User findByEmail(final String email) {
        return records
                .stream()
                .filter(m -> email.equals(m.getEmail()))
                .findFirst().orElse(null);
    }

    @Override
    public Boolean isLoginExist(final String login) {
        return records
                .stream()
                .anyMatch(m -> login.equals(m.getLogin()));
    }

    @Override
    public Boolean isEmailExist(final String email) {
        return records
                .stream()
                .anyMatch(m -> email.equals(m.getEmail()));
    }

    @Override
    public User lockUserByLogin(final String login) {
        final User user = findByLogin(login);
        user.setLocked(true);
        return user;
    }

    @Override
    public User unlockUserByLogin(final String login) {
        final User user = findByLogin(login);
        user.setLocked(false);
        return user;
    }
}
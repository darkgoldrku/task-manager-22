package ru.t1.bugakov.tm.repository;

import ru.t1.bugakov.tm.api.repository.IAbstractUserOwnedRepository;
import ru.t1.bugakov.tm.model.AbstractUserOwnedModel;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public abstract class AbstractUserOwnedRepository<M extends AbstractUserOwnedModel> extends AbstractRepository<M> implements IAbstractUserOwnedRepository<M> {

    @Override
    public M add(final String userId, final M model) {
        model.setUserId(userId);
        records.add(model);
        return model;
    }

    @Override
    public List<M> findAll(final String userId) {
        return records
                .stream()
                .filter(m -> userId.equals(m.getUserId()))
                .collect(Collectors.toList());
    }

    @Override
    public List<M> findAll(final String userId, final Comparator<M> comparator) {
        return records
                .stream()
                .filter(m -> userId.equals(m.getUserId()))
                .sorted(comparator)
                .collect(Collectors.toList());
    }

    @Override
    public int getSize(final String userId) {
        return (int) records
                .stream()
                .filter(m -> userId.equals(m.getUserId()))
                .count();
    }

    @Override
    public M findById(final String userId, final String id) {
        return records
                .stream()
                .filter(m -> userId.equals(m.getUserId()))
                .filter(m -> id.equals(m.getId()))
                .findFirst().orElse(null);
    }

    @Override
    public M findByIndex(final String userId, final Integer index) {
        return records
                .stream()
                .filter(m -> userId.equals(m.getUserId()))
                .skip(index)
                .findFirst().orElse(null);
    }

    @Override
    public boolean existsById(final String userId, final String id) {
        return findById(userId, id) != null;
    }

    @Override
    public void clear(final String userId) {
        final List<M> models = findAll(userId);
        models.clear();
    }

    @Override
    public M remove(final String userId, final M model) {
        if (userId == null || model == null) return null;
        records.remove(model);
        return model;
    }

    @Override
    public M removeById(final String userId, final String id) {
        if (userId == null || id == null) return null;
        final M model = findById(userId, id);
        if (model == null) return null;
        return remove(model);
    }

    @Override
    public M removeByIndex(final String userId, final Integer index) {
        final M model = findByIndex(userId, index);
        if (model == null) return null;
        return remove(model);
    }

}
